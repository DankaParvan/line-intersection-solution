package com.epam.rd.autotasks.intersection;

public class Line {
    private int k;
    private int b;

    public Line(int k, int b) {
        this.k = k;
        this.b = b;
    }

    public int getK() {
        return k;
    }

    public int getB() {
        return b;
    }

    public Point getIntersection(Line line2) {
        if (line2 == null) {
            return null;
        }

        Integer x = getIntersectionX(line2);
        if (x == null) {
            return null;
        }
        return new Point(x, getK() * x + b);
    }


    private Integer getIntersectionX(Line line2) {
        if (getK() == line2.getK() && getB() != line2.getB()) {
            return null;
        }
        if (getK() == line2.getK() && getB() == line2.getB()) {
            return null;
        }
        return (((line2.getB() - getB()) / (getK() - line2.getK())));
    }


}
